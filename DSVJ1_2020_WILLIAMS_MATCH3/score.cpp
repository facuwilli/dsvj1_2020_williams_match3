#include "score.h"

Score::Score(Vector2 pos, int size, Color color)
{
	position = pos;
	scoreSize = size;
	scoreColor = color;
	ResetScore();
}
void Score::Draw()
{
	DrawText(score, static_cast<int>(position.x), static_cast<int>(position.y), scoreSize, scoreColor);
}
void Score::ResetScore()
{
	for(short i = 0; i < maxScore; i++)
	{
		score[i] = 48;
	}
}
void Score::AddScore(short points[maxScore])
{
	for(short index = maxScore - 1; index >= (maxScore - maxScore); index--)
	{
		score[index] += points[index] - ASCII_cero;
	}

	for(short index = maxScore - 1; index >= (maxScore - maxScore); index--)
	{
		if(score[index] > ASCII_nueve)
		{
			score[index] -= 10;
			score[index - 1] += 1;
		}
	}
}
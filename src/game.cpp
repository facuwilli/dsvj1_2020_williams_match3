#include "game.h"

Game::Game()
{
	screenWidth = 800;
	screenHeight = 450;	
	InitWindow(screenWidth, screenHeight, "GAME");
	SetTargetFPS(fps);
}
Game::~Game()
{
	CloseWindow();
}
void Game::SetScreenWidth(short value)
{
	screenWidth = value;
}
void Game::SetScreenHeight(short value)
{
	screenHeight = value;
}
short Game::GetScreenWidth()
{
	return screenWidth;
}
short Game::GetScreenHeight()
{
	return screenHeight;
}
void Game::Init()
{
	
}
void Game::Input()
{

}
void Game::Update()
{

}
void Game::Draw()
{
	BeginDrawing();

	ClearBackground(RAYWHITE);	

	EndDrawing();
}
void Game::Deinit()
{

}
void Game::Play()
{
	Init();

	while (!WindowShouldClose())
	{
		Input();
		Update();
		Draw();	
	}

	Deinit();
}
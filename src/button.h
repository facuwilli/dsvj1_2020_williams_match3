#ifndef BUTTON_H

#define BUTTON_H

#include "raylib.h"
#include "scene_manager_class.h"

class Button
{
private:
	Rectangle rec;
	Color recColor;
	const char* text;
	int textX;
	int textY;
	int textFontSize;
	Color textColor;
public:
	Button();
	Button(int recX, int recY, int recW, int recH, const char* _text, int _textX, int _textY, int textFnt);
	void SetRec(int recX, int recY, int recW, int recH);
	void SetRecColor(Color color);
	void SetText(const char* _text);
	void SetTextX(int value);
	void SetTextY(int value);
	void SetTextFontSize(int value);
	void SetTextColor(Color color);
	Rectangle GetRec();
	Color GetRecColor();
	const char* GetText();
	int GetTextX();
	int GetTextY();
	int GetTextFontSize();
	Color GetTextColor();
	bool mouseOnButton(int leftX, int rightX, int upY, int downY);
	void HighlightButton();
	void DrawButton();
	Scenes ChangeSceneOnClick(Scenes currentScene, Scenes nextScene, SceneManager* sceneManager);
	//void ResetGameOnClick(Scenes* scene, Spawner* spawner, Player* player, MainScenes newScene, Music gameMusic);
};

#endif

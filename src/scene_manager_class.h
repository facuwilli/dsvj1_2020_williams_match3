#ifndef SCENE_MANAGER_CLASS_H
#define SCENE_MANAGER_CLASS_H

#include "enum_scenes.h" 

class SceneManager
{
private:
	Scenes currentScene;
	Scenes lastScene;	
	bool onScene;
public:
	SceneManager();
	~SceneManager();
	void SetCurrentScene(Scenes scene);
	void SetLastScene(Scenes scene);
	void SetOnScene(bool value);
	Scenes GetCurrentScene();
	Scenes GetLastScene();
	bool GetOnScene();
};

#endif

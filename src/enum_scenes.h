#ifndef SCENES_H
#define SCENES_H

enum class Scenes{game, menu, credits, exit, pause, win, loose};

#endif

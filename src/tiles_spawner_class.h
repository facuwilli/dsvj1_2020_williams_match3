#ifndef TILE_SPAWNER_CLASS_H
#define TILE_SPAWNER_CLASS_H

#include <iostream>
#include <math.h>
#include <vector>
#include "tile_class.h"

using namespace std;

class Spawner
{
private:
	static const short maxFil = 6;
	static const short maxCol = 8;	
	const short tilesNecessaryToMatch = 3;	
	const short mouseInteractionButton = MOUSE_LEFT_BUTTON;	
	const short screenWidth = GetScreenWidth();
	const short screenHeight = GetScreenHeight() / 2;
	const short xSeparation = GetScreenWidth() / 70;
	const short ySeparation = GetScreenHeight() / 70;
	Tile* tiles[maxFil][maxCol];	
	vector<Vector2> selectedTiles;		
	Texture2D blueGem;
	Texture2D greenGem;
	Texture2D redGem;
	Texture2D yellowGem;	
	static const short maxShiftsIndex = 2;
	char shifts[maxShiftsIndex];	
public:	
	Spawner();
	~Spawner();
	void Init();
	void Input();	
	void Draw();	
	void Deinit();
	Tile* SetUpTile(Vector2 index, short width, short height, short xSeparation, short ySeparation);	
	void SelectTile();
	bool IsTileSelected(Vector2 pos);
	bool AreTilesConsecutive(short x1, short x2, short y1, short y2);
	bool DoTilesMatchColor(short x1, short x2, short y1, short y2);
	void ResetSelectedTiles();		
	void DestroySelectedTiles();
	void DrawTiles();
	void DrawVisualHelpOfSelectedTiles(Color color);
	void ReplacedDestoyedBlocks();
	
	void ResetShifts();
	void AddShift();
	char* GetShifts();
	char GetFirstValueOfArray();
	char GetSecondValueOfArray();
};

#endif


#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "scene_manager_class.h"
#include "tiles_spawner_class.h"
#include "button.h"

class Game
{
private:
	short screenWidth;
	short screenHeight;
	const int fps = 60;	
	SceneManager* sceneManager;
	bool close;	
	short exitGameKey;
	Spawner* spawner;
	short pauseGameKey;
	Texture2D background;	
	static const short maxPointsIndex = 5;
	char necesarryPointsToWin[maxPointsIndex];
	static const short maxShiftsIndex = 2;
	char maxShifts[maxShiftsIndex];
	Button* startB;
	Button* exitB;
	Button* creditsB;	
	Texture2D menuBackgroundTextr;
	Music menuMusic;
	Music matchMusic;
	Button* resetGameB;	
	Button* goMenuB;
public:
	Game();
	~Game();
	void SetScreenWidth(short value);
	void SetScreenHeight(short value);
	void SetClose(bool value);	
	void SetExitGameKey(short key);
	void SetPauseGameKey(short key);
	short GetScreenWidth();
	short GetScreenHeight();
	bool GetClose();
	short GetExitGameKey();
	short GetPauseGameKey();
	void Init();
	void Input();
	void Update();
	void Draw();
	void Deinit();
	void Play();
	void ExitWithSpace();
	void PlayMenuMusic();
};

#endif

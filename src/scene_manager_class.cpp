#include "scene_manager_class.h"

SceneManager::SceneManager()
{	
	SetCurrentScene(Scenes::menu);
	SetLastScene(Scenes::menu);
	SetOnScene(true);	
}
SceneManager::~SceneManager()
{

}

void SceneManager::SetCurrentScene(Scenes scene)
{
	currentScene = scene;
}
void SceneManager::SetLastScene(Scenes scene)
{
	lastScene = scene;
}
void SceneManager::SetOnScene(bool value)
{
	onScene = value;
}

Scenes SceneManager::GetCurrentScene()
{
	return currentScene;
}
Scenes SceneManager::GetLastScene()
{
	return lastScene;
}
bool SceneManager::GetOnScene()
{
	return onScene;
}
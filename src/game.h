#ifndef GAME_H
#define GAME_H

#include "raylib.h"

class Game
{
private:
	short screenWidth;
	short screenHeight;
	const int fps = 60;	
public:
	Game();
	~Game();
	void SetScreenWidth(short value);
	void SetScreenHeight(short value);
	short GetScreenWidth();
	short GetScreenHeight();
	void Init();
	void Input();
	void Update();
	void Draw();
	void Deinit();
	void Play();
};

#endif

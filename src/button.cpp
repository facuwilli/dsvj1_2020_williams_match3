#include "button.h"

Button::Button()
{
	rec.x = 600;
	rec.y = 200;
	rec.width = 250;
	rec.height = 100;
	recColor = RED;
	text = " ";
	textX = 620;
	textY = 220;
	textFontSize = 30;
	textColor = BLACK;
}
Button::Button(int recX, int recY, int recW, int recH, const char* _text, int _textX, int _textY, int textFnt)
{
	rec.x = recX;
	rec.y = recY;
	rec.width = recW;
	rec.height = recH;
	text = _text;
	textX = _textX;
	textY = _textY;
	textFontSize = textFnt;
	recColor = RED;
	textColor = BLACK;
}
void Button::SetRec(int recX, int recY, int recW, int recH)
{
	rec.x = recX;
	rec.y = recY;
	rec.width = recW;
	rec.height = recH;
}
void Button::SetRecColor(Color color)
{
	recColor = color;
}
void Button::SetText(const char* _text)
{
	text = _text;
}
void Button::SetTextX(int value)
{
	textX = value;
}
void Button::SetTextY(int value)
{
	textY = value;
}
void Button::SetTextFontSize(int value)
{
	textFontSize = value;
}
void Button::SetTextColor(Color color)
{
	textColor = color;
}
Rectangle Button::GetRec()
{
	return rec;
}
Color Button::GetRecColor()
{
	return recColor;
}
const char* Button::GetText()
{
	return text;
}
int Button::GetTextX()
{
	return textX;
}
int Button::GetTextY()
{
	return textY;
}
int Button::GetTextFontSize()
{
	return textFontSize;
}
Color Button::GetTextColor()
{
	return textColor;
}
bool Button::mouseOnButton(int leftX, int rightX, int upY, int downY)
{
	return (GetMousePosition().x >= leftX) && (GetMousePosition().x <= rightX)
		&& (GetMousePosition().y <= downY) && (GetMousePosition().y >= upY);
}
void Button::HighlightButton()
{
	if (mouseOnButton(rec.x, rec.x + rec.width, rec.y, rec.y + rec.height))
	{
		SetRecColor(YELLOW);
		SetTextColor(BLACK);
	}
	else
	{
		SetRecColor(RED);
		SetTextColor(BLUE);
	}
}
void Button::DrawButton()
{
	HighlightButton();

	DrawRectangle(rec.x, rec.y, rec.width, rec.height, recColor);
	DrawText(text, textX, textY, textFontSize, textColor);
}
Scenes Button::ChangeSceneOnClick(Scenes currentScene, Scenes nextScene, SceneManager* sceneManager)
{
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && mouseOnButton(rec.x, rec.x + rec.width, rec.y, rec.y + rec.height))
	{
		sceneManager->SetLastScene(sceneManager->GetCurrentScene());
		sceneManager->SetOnScene(false);
		return nextScene;
	}
	else
	{
		return currentScene;
	}
}
/*void Button::ResetGameOnClick(Scenes* scene, Spawner* spawner, Player* player, MainScenes newScene, Music gameMusic)
{
	if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && mouseOnButton(rec.x, rec.x + rec.width, rec.y, rec.y + rec.height))
	{
		scene->ChangeCurrentScene(newScene);
		spawner->Reset();
		player->Reset();
		StopMusicStream(gameMusic);
	}
}*/
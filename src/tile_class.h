#ifndef TILE_CLASS_H
#define TILE_CLASS_H

#include "raylib.h"
#include "enum_tile_type.h"

class Tile
{
private:
	Rectangle rec;	
	Color color;
	TileType type;	
	Texture2D tileTexture;
	static const short maxTilesIndex = 3;
	static char tilesDestroyed[maxTilesIndex];
public:
	Tile();
	~Tile();
	void SetRectangle(float _x, float _y, float _width, float _height);
	void SetX(float x);
	void SetY(float y);
	void SetWidth(float width);
	void SetHeight(float height);	
	void SetColor(Color _color);
	void SetColor(TileType type);
	void SetType(TileType _type);
	void SetTexture(Texture2D Texture);
	TileType GenerateRandomType();	
	Rectangle GetRectangle();
	float GetX();
	float GetY();
	float GetWidth();
	float GetHeight();
	Color GetColor();	
	TileType GetType();
	Texture2D GetTexture();
	bool IsMouseAbove();
	static void AddTileDestroyed();
	static void ResetTilesDestroyed();
	static char* GetTilesDestroyed();
	static char GetFirstValueOfArray();
	static char GetSecondValueOfArray();
	static char GetThirdValueOfArray();
};

#endif

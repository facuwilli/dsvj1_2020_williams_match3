#include "game_class.h"

Game::Game()
{	
	SetScreenWidth(1080);
	SetScreenHeight(720);
	InitWindow(screenWidth, screenHeight, "GEMS MATCH 3");
	SetTargetFPS(fps);
	sceneManager = new SceneManager();
	SetClose(false);	
	SetExitGameKey(KEY_E);
	spawner = NULL;
	SetPauseGameKey(KEY_P);
	background = LoadTexture("../res/textures/back.png");
	background.width = screenWidth;
	background.height = screenHeight / 2 - 15;	
	maxShifts[0] = 50;
	maxShifts[1] = 49;
	necesarryPointsToWin[0] = 49;
	necesarryPointsToWin[1] = 50;
	necesarryPointsToWin[2] = 49;
	startB = NULL;
	creditsB = NULL;
	exitB = NULL;		
	menuBackgroundTextr = LoadTexture("../res/textures/background.png");
	menuBackgroundTextr.width = screenWidth;
	menuBackgroundTextr.height = screenHeight;
	menuMusic = LoadMusicStream("../res/music/menu.mp3");
	SetMusicVolume(menuMusic, 0.4f);
	matchMusic = LoadMusicStream("../res/music/match.mp3");
	SetMusicVolume(matchMusic, 0.4f);
	InitAudioDevice();
	resetGameB = NULL;
	goMenuB = NULL;
}
Game::~Game()
{
	CloseWindow();
	delete sceneManager; sceneManager = NULL;
	delete spawner; spawner = NULL;	
	UnloadTexture(background);
	UnloadTexture(menuBackgroundTextr);
	CloseAudioDevice();
	UnloadMusicStream(menuMusic);
	UnloadMusicStream(matchMusic);
}

void Game::SetScreenWidth(short value)
{
	screenWidth = value;
}
void Game::SetScreenHeight(short value)
{
	screenHeight = value;
}
void Game::SetClose(bool value)
{
	close = value;
}
void Game::SetExitGameKey(short key)
{
	exitGameKey = key;
}
void Game::SetPauseGameKey(short key)
{
	pauseGameKey = key;
}

short Game::GetScreenWidth()
{
	return screenWidth;
}
short Game::GetScreenHeight()
{
	return screenHeight;
}
bool Game::GetClose()
{
	return close;
}
short Game::GetExitGameKey()
{
	return exitGameKey;
}
short Game::GetPauseGameKey()
{
	return pauseGameKey;
}

void Game::Init()
{
	sceneManager->SetLastScene(sceneManager->GetCurrentScene());
	if(sceneManager->GetOnScene() == false)
	{
		sceneManager->SetOnScene(true);
	}

	switch (sceneManager->GetCurrentScene())
	{
	case Scenes::game:	

		PlayMusicStream(matchMusic);

		if (IsMusicPlaying(menuMusic)) 
		{
			StopMusicStream(menuMusic);
		}
		
		if(spawner == NULL)
		{
			spawner = new Spawner();
			spawner->Init();
		}		

		break;
	case Scenes::menu:		
		
		if (startB == NULL) 
		{
			startB = new Button(
				(screenWidth / 2) - ((screenWidth / 5) / 2),
				((screenHeight / 4) + (screenHeight / 9) * 0),
				screenWidth / 5,
				screenHeight / 9,
				"START",
				(screenWidth / 2) - ((screenWidth / 5) / 2) + ((30 * 5) / 4),
				((screenHeight / 4) + (screenHeight / 9) * 0) + ((30 * 5) / 10),
				(screenHeight / 25));
		}

		if (creditsB == NULL) {
			creditsB = new Button(
				(screenWidth / 2) - ((screenWidth / 5) / 2),
				((screenHeight / 4) + (screenHeight / 9) * 2),
				screenWidth / 5,
				screenHeight / 9,
				"CREDITS",
				(screenWidth / 2) - ((screenWidth / 5) / 2) + ((30 * 6) / 4),
				((screenHeight / 4) + (screenHeight / 9) * 2) + ((30 * 6) / 10),
				(screenHeight / 25));
		}

		if (exitB == NULL) 
		{
			exitB = new Button(
				(screenWidth / 2) - ((screenWidth / 5) / 2),
				((screenHeight / 4) + (screenHeight / 9) * 4),
				screenWidth / 5,
				screenHeight / 9,
				"EXIT",
				(screenWidth / 2) - ((screenWidth / 5) / 2) + ((30 * 4) / 4),
				((screenHeight / 4) + (screenHeight / 9) * 4) + ((30 * 4) / 10),
				(screenHeight / 25));
		}
		break;
	case Scenes::pause:

		if (resetGameB == NULL) 
		{
			resetGameB = new Button(
				(screenWidth / 2) - ((screenWidth / 5) / 2),
				((screenHeight / 4) + (screenHeight / 9) * 0),
				screenWidth / 5,
				screenHeight / 9,
				"RESET",
				(screenWidth / 2) - ((screenWidth / 5) / 2) + ((30 * 5) / 4),
				((screenHeight / 4) + (screenHeight / 9) * 0) + ((30 * 5) / 10),
				(screenHeight / 25));
		}

		if (goMenuB == NULL) 
		{
			goMenuB = new Button(
				(screenWidth / 2) - ((screenWidth / 5) / 2),
				((screenHeight / 4) + (screenHeight / 9) * 2),
				screenWidth / 5,
				screenHeight / 9,
				"MENU",
				(screenWidth / 2) - ((screenWidth / 5) / 2) + ((30 * 6) / 4),
				((screenHeight / 4) + (screenHeight / 9) * 2) + ((30 * 6) / 10),
				(screenHeight / 25));
		}

		break;
	default:
		break;
	}
}
void Game::Input()
{
	if (IsKeyPressed(GetExitGameKey()))
	{
		SetClose(true);
		sceneManager->SetOnScene(false);
	}

	switch (sceneManager->GetCurrentScene())
	{
	case Scenes::game:

		if (sceneManager != NULL && spawner != NULL)
		{			
			if (IsKeyPressed(GetPauseGameKey()))
			{
				sceneManager->SetOnScene(false);
				sceneManager->SetLastScene(sceneManager->GetCurrentScene());
				sceneManager->SetCurrentScene(Scenes::pause);				
			}

			spawner->Input();
		}		

		break;
	case Scenes::menu:

		if (sceneManager != NULL) 
		{
			sceneManager->SetCurrentScene(startB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), Scenes::game, sceneManager));
			sceneManager->SetCurrentScene(creditsB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), Scenes::credits, sceneManager));
			sceneManager->SetCurrentScene(exitB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), Scenes::exit, sceneManager));
		}

		break;
	case Scenes::credits:

		ExitWithSpace();

		break;
	case Scenes::pause:
		
		sceneManager->SetCurrentScene(resetGameB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), Scenes::game, sceneManager));
		sceneManager->SetCurrentScene(goMenuB->ChangeSceneOnClick(sceneManager->GetCurrentScene(), Scenes::credits, sceneManager));

		break;
	case Scenes::win:

		ExitWithSpace();

		break;
	case Scenes::loose:
		
		ExitWithSpace();

		break;
	default:
		break;
	}
}
void Game::Update()
{
	switch (sceneManager->GetCurrentScene())
	{
	case Scenes::game:	

		UpdateMusicStream(matchMusic);

		if (sceneManager != NULL && spawner != NULL)
		{
			if (Tile::GetFirstValueOfArray() >= necesarryPointsToWin[0]
				&& Tile::GetSecondValueOfArray() >= necesarryPointsToWin[1]
				&& Tile::GetThirdValueOfArray() >= necesarryPointsToWin[2])
			{				
				sceneManager->SetOnScene(false);
				sceneManager->SetLastScene(sceneManager->GetCurrentScene());
				sceneManager->SetCurrentScene(Scenes::win);
			}
			else if (spawner->GetFirstValueOfArray() >= maxShifts[0] && spawner->GetSecondValueOfArray() >= maxShifts[1])
			{				
				sceneManager->SetOnScene(false);
				sceneManager->SetLastScene(sceneManager->GetCurrentScene());
				sceneManager->SetCurrentScene(Scenes::loose);
			}
		}
		
		break;
	case Scenes::menu:

		PlayMenuMusic();

		break;
	case Scenes::credits:

		PlayMenuMusic();

		break;
	case Scenes::exit:
		sceneManager->SetLastScene(Scenes::exit);
		SetClose(true);	
		sceneManager->SetOnScene(false);
		break;
	default:
		break;
	}
}
void Game::Draw()
{
	switch (sceneManager->GetCurrentScene())
	{
	case Scenes::game:

		if (spawner != NULL)
		{
			BeginDrawing();

			ClearBackground(BLACK);

			DrawTexture(background, 0, 0, WHITE);
			DrawRectangle(0, screenHeight / 2 - 15, screenWidth, screenHeight / 2 + 15, GRAY);

			spawner->Draw();

			DrawText("MATCH 3", (GetScreenWidth() / 2) - (GetScreenWidth() / 6), 50, 80, BLACK);

			DrawRectangle(18, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 55, 360, 90, GRAY);

			DrawText("Shifts: ", 20, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 50, 30, BLACK);
			DrawText(spawner->GetShifts(), 140, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 50, 30, BLACK);

			DrawText("Tiles Destroyed: ", 20, (GetScreenHeight() / 2) - (GetScreenHeight() / 10), 30, BLACK);
			DrawText(Tile::GetTilesDestroyed(), 290, (GetScreenHeight() / 2) - (GetScreenHeight() / 10), 30, BLACK);

			DrawRectangle(790, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 55, 260, 90, GRAY);

			DrawText("Max Shifts: ", 800, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 50, 30, BLACK);
			DrawText(maxShifts, 980, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 50, 30, BLACK);

			DrawText("Objetive: ", 800, (GetScreenHeight() / 2) - (GetScreenHeight() / 10), 30, BLACK);
			DrawText(necesarryPointsToWin, 950, (GetScreenHeight() / 2) - (GetScreenHeight() / 10), 30, BLACK);

			DrawText("PAUSE (P)", screenWidth / 2, (GetScreenHeight() / 2) - (GetScreenHeight() / 10), 30, BLACK);
			DrawText("CLOSE (E)", screenWidth / 2, (GetScreenHeight() / 2) - (GetScreenHeight() / 10) - 40, 30, BLACK);

			EndDrawing();
		}		

		break;
	case Scenes::menu:

		if(startB!=NULL && creditsB != NULL && exitB != NULL)
		{
			BeginDrawing();

			ClearBackground(BLACK);

			DrawTexture(menuBackgroundTextr, 1, 1, WHITE);
			
			DrawText("MATCH 3", (GetScreenWidth() / 2) - (GetScreenWidth() / 6), 50, 80, BLACK);
			DrawText("v1.0", screenHeight - (5 * 30), screenHeight - 35, 30, BLACK);

			startB->DrawButton();
			creditsB->DrawButton();
			exitB->DrawButton();

			EndDrawing();
		}

		break;
	case Scenes::credits:

		BeginDrawing();

		ClearBackground(BLACK);
		
		DrawTexture(menuBackgroundTextr, 1, 1, WHITE);

		DrawRectangle(15, 245, 920, 350, GRAY);

		DrawText("MATCH 3", (GetScreenWidth() / 2) - (GetScreenWidth() / 6), 50, 80, BLACK);
		DrawText("v1.0", screenHeight - (5 * 30), screenHeight - 35, 30, BLACK);
		DrawText("CREDITS", (GetScreenWidth() / 2) - (7 * 30), 150, 30, BLACK);
		DrawText("GAME PROGRAMED BY FACUNDO WILLIAMS", 20, 250, 30, BLACK);
		DrawText("ASSETS:", 20, 300, 30, BLACK);
		DrawText("itchio-Free Pixel Art Hill", 20, 340, 30, BLACK);
		DrawText("https://opengameart.org/content/happy-arcade-tune", 20, 380, 30, BLACK);
		DrawText("https://opengameart.org/content/town-theme-rpg", 20, 420, 30, BLACK);
		DrawText("https://itch.io/game-assets/free/tag-gems", 20, 460, 30, BLACK);
		DrawText("https://opengameart.org/", 20, 500, 30, BLACK);

		DrawText("PRESS ENTER TO GO TO MENU", 20, 540, 30, BLACK);

		EndDrawing();

		break;

	case Scenes::pause:

		BeginDrawing();

		ClearBackground(BLACK);

		DrawTexture(menuBackgroundTextr, 1, 1, WHITE);

		DrawText("GAME PAUSED", (screenWidth / 2) - ((17 * 30) / 4), (screenHeight / 10), 35, GREEN);
		if (resetGameB != NULL) { 
			resetGameB->DrawButton(); 
		}
		
		if (goMenuB != NULL) 
		{ 
			goMenuB->DrawButton(); 		
		}

		EndDrawing();

		break;
	case Scenes::win:

		BeginDrawing();

		ClearBackground(BLACK);

		DrawTexture(menuBackgroundTextr, 1, 1, WHITE);

		DrawText("MATCH 3", (GetScreenWidth() / 2) - (GetScreenWidth() / 6), 50, 80, BLACK);
		DrawText("v1.0", screenHeight - (5 * 30), screenHeight - 35, 30, BLACK);

		DrawText("YOU WIN", (screenWidth / 2) - 150, screenHeight / 2, 60, BLACK);

		DrawText("PRESS ENTER TO GO TO MENU", 20, 580, 30, BLACK);

		EndDrawing();

		break;
	case Scenes::loose:

		BeginDrawing();

		ClearBackground(BLACK);

		DrawTexture(menuBackgroundTextr, 1, 1, WHITE);

		DrawText("MATCH 3", (GetScreenWidth() / 2) - (GetScreenWidth() / 6), 50, 80, BLACK);
		DrawText("v1.0", screenHeight - (5 * 30), screenHeight - 35, 30, BLACK);

		DrawText("YOU LOOSE", (screenWidth / 2) - 150, screenHeight / 2, 60, BLACK);

		DrawText("PRESS ENTER TO GO TO MENU", 20, 580, 30, BLACK);

		EndDrawing();

		break;
	default:
		break;
	}	
}
void Game::Deinit()
{
	switch (sceneManager->GetLastScene())
	{
	case Scenes::game:

		StopMusicStream(matchMusic);

		if(spawner != NULL)
		{
			spawner->Deinit();

			delete spawner;
			spawner = NULL;
		}
		
		break;
	case Scenes::menu:
		
		if(startB != NULL)
		{
			delete startB;
			startB = NULL;
		}

		if(creditsB != NULL)
		{
			delete creditsB;
			creditsB = NULL;
		}

		if(exitB != NULL)
		{
			delete exitB;
			exitB = NULL;
		}

		break;
	case Scenes::pause:

		if(resetGameB != NULL)
		{
			delete resetGameB;
			resetGameB = NULL;
		}

		if(goMenuB != NULL)
		{
			delete goMenuB;
			goMenuB = NULL;
		}

		break;
	default:
		break;
	}
}

void Game::Play()
{
	while (!GetClose())
	{
		Init();

		while(sceneManager->GetOnScene())
		{
			Input();
			Update();
			Draw();
		}	
		
		Deinit();
	}	
}

void Game::ExitWithSpace()
{
	if (IsKeyPressed(KEY_ENTER) && sceneManager != NULL)
	{
		sceneManager->SetOnScene(false);
		sceneManager->SetCurrentScene(Scenes::menu);
	}
}
void Game::PlayMenuMusic()
{
	if (!IsMusicPlaying(menuMusic))
	{
		PlayMusicStream(menuMusic);
	}

	UpdateMusicStream(menuMusic);
}
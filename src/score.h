#ifndef SCORE_H
#define SCORE_H

#include "raylib.h"

class Score
{
private:
	const short ASCII_cero = 48;
	const short ASCII_nueve = 57;
	static const short maxScore = 7;
	char score[maxScore];
	Vector2 position;
	int scoreSize;
	Color scoreColor;	
public:
	Score(Vector2 pos, int size, Color color);
	void Draw();
	void ResetScore();
	void AddScore(char points[maxScore], short tilesDestroyed);
};

#endif

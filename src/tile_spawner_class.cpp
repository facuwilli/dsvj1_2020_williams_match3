#include "tiles_spawner_class.h"

Spawner::Spawner()
{
	for (short y = 0; y < maxFil; y++)
	{
		for(short x = 0; x <  maxCol; x++)
		{
			tiles[y][x] = NULL;
		}
	}	

	blueGem = LoadTexture("../res/textures/blue_gem.png");
	greenGem = LoadTexture("../res/textures/green_gem.png");;
	redGem = LoadTexture("../res/textures/red_gem.png");
	yellowGem = LoadTexture("../res/textures/yellow_gem.png");

	ResetSelectedTiles();	
}
Spawner::~Spawner()
{
	for (short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{
			if (tiles[y][x] != NULL)
			{
				delete tiles[y][x];
				tiles[y][x] = NULL;
			}
		}
	}
}
void Spawner::Init(){
	
	blueGem = LoadTexture("../res/textures/blue_gem.png");
	greenGem = LoadTexture("../res/textures/green_gem.png");;
	redGem = LoadTexture("../res/textures/red_gem.png");
	yellowGem = LoadTexture("../res/textures/yellow_gem.png");

	for (short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{
			tiles[y][x] = SetUpTile( {static_cast<float>(x),static_cast<float>(y)}, screenWidth, screenHeight, xSeparation, ySeparation);
		}
	}	
	
	ResetSelectedTiles();
	Tile::ResetTilesDestroyed();
	ResetShifts();
}
void Spawner::Input()
{
	if (IsMouseButtonDown(mouseInteractionButton))
	{
		SelectTile();
	}
	else if (IsMouseButtonReleased(mouseInteractionButton) && selectedTiles.size() >= tilesNecessaryToMatch)
	{
		AddShift();

		DestroySelectedTiles();

		ReplacedDestoyedBlocks();

		ResetSelectedTiles();		
	}
	else
	{
		ResetSelectedTiles();
	}
}
void Spawner::Draw()
{
	DrawTiles();
	DrawVisualHelpOfSelectedTiles(BLACK);
}
Tile* Spawner::SetUpTile(Vector2 index, short width, short height, short xSeparation, short ySeparation)
{
	Tile* newTile = NULL;	
	short x = static_cast<short>(index.x);
	short y = static_cast<short>(index.y);		

	newTile = new Tile();
	newTile->SetWidth(static_cast<float>((width - (xSeparation * (maxCol + 1))) / maxCol));
	newTile->SetX((newTile->GetWidth() * index.x) + (xSeparation * (index.x + 1)));
	newTile->SetHeight(static_cast<float>((height - (ySeparation * maxFil)) / maxFil));
	newTile->SetY(height + (newTile->GetHeight() * index.y) + (ySeparation * index.y));	
	newTile->SetType(newTile->GenerateRandomType());
	newTile->SetColor(newTile->GetType());
	switch (newTile->GetType())
	{
	case TileType::red:		
		redGem.width = newTile->GetWidth();
		redGem.height = newTile->GetHeight();
		newTile->SetTexture(redGem);
		break;
	case TileType::green:		
		greenGem.width = newTile->GetWidth();
		greenGem.height = newTile->GetHeight();
		newTile->SetTexture(greenGem);
		break;
	case TileType::blue:		
		blueGem.width = newTile->GetWidth();
		blueGem.height = newTile->GetHeight();
		newTile->SetTexture(blueGem);
		break;
	case TileType::yellow:		
		yellowGem.width = newTile->GetWidth();
		yellowGem.height = newTile->GetHeight();
		newTile->SetTexture(yellowGem);
		break;
	}	

	return newTile;
}
void Spawner::Deinit()
{
	for(short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{
			if(tiles[y][x] != NULL)
			{
				delete tiles[y][x]; 
				tiles[y][x] = NULL;
			}			
		}
	}

	UnloadTexture(blueGem);
	UnloadTexture(greenGem);
	UnloadTexture(redGem);
	UnloadTexture(yellowGem);
}
void Spawner::SelectTile()
{
	for (short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{
			if (tiles[y][x] != NULL)
			{
				if(selectedTiles.size() != 0)
				{
					if(tiles[y][x]->IsMouseAbove() && !IsTileSelected({ static_cast<float>(x), static_cast<float>(y) }) 
						&& AreTilesConsecutive(static_cast<short>(selectedTiles[selectedTiles.size() - 1].x), x, static_cast<short>(selectedTiles[selectedTiles.size() - 1].y), y)
						&& DoTilesMatchColor(static_cast<short>(selectedTiles[selectedTiles.size() - 1].x), x, static_cast<short>(selectedTiles[selectedTiles.size() - 1].y), y))
					{
						selectedTiles.push_back({ static_cast<float>(x), static_cast<float>(y) });
					}					
				}
				else if(tiles[y][x]->IsMouseAbove())
				{
					selectedTiles.push_back({ static_cast<float>(x), static_cast<float>(y) });
				}							

				if(selectedTiles.size() >= 2)
				{			
					if(tiles[static_cast<short>(selectedTiles[selectedTiles.size() - 2].y)][static_cast<short>(selectedTiles[selectedTiles.size() - 2].x)]->IsMouseAbove())
					{
						selectedTiles.back();
						selectedTiles.pop_back();	
					}				
				}			
			}
		}
	}
}
bool Spawner::IsTileSelected(Vector2 pos)
{
	bool result = false;

	for (short i = 0; i < selectedTiles.size(); i++)
	{
		if (selectedTiles[i].x == pos.x && selectedTiles[i].y == pos.y)
		{
			result = true;
			break;
		}
		else
		{
			result = false;
		}
	}

	return result;
}
bool Spawner::AreTilesConsecutive(short x1, short x2, short y1, short y2)
{		
	return (((x1 == x2) && ((y1 + 1 == y2) || (y1 - 1 == y2)))
		|| ((y1 == y2) && ((x1 + 1 == x2) || (x1 - 1 == x2)))
		|| (((x1 + 1 == x2) || (x1 - 1 == x2)) && ((y1 + 1 == y2) || (y1 - 1 == y2))));
}
bool Spawner::DoTilesMatchColor(short x1, short x2, short y1, short y2)
{
	return tiles[y1][x1]->GetType() == tiles[y2][x2]->GetType();
}
void Spawner::ResetSelectedTiles()
{	
	if(!selectedTiles.empty())
	{
		selectedTiles.resize(0);
	}
}
void Spawner::DestroySelectedTiles()
{	
	for (short tilesIndex = 0; tilesIndex < selectedTiles.size(); tilesIndex++)
	{
		if (tiles[static_cast<short>(selectedTiles[tilesIndex].y)][static_cast<short>(selectedTiles[tilesIndex].x)] != NULL)
		{			
			delete tiles[static_cast<short>(selectedTiles[tilesIndex].y)][static_cast<short>(selectedTiles[tilesIndex].x)];
			tiles[static_cast<short>(selectedTiles[tilesIndex].y)][static_cast<short>(selectedTiles[tilesIndex].x)] = NULL;
		}
	}		
}
void Spawner::DrawTiles()
{
	for (short y = 0; y < maxFil; y++)
	{
		for (short x = 0; x < maxCol; x++)
		{
			if (tiles[y][x] != NULL)
			{
				DrawTexture(tiles[y][x]->GetTexture(), 
					static_cast<int>(tiles[y][x]->GetX()), 
					static_cast<int>(tiles[y][x]->GetY()),
					tiles[y][x]->GetColor());
			}
		}
	}
}
void Spawner::DrawVisualHelpOfSelectedTiles(Color color)
{
	short xIndex = 0;
	short yIndex = 0;
	int xPos = 0;
	int yPos = 0;
	short radius = 0;

	for(short i = 0; i < selectedTiles.size(); i++)
	{
		xIndex = static_cast<short>(selectedTiles[i].x);
		yIndex = static_cast<short>(selectedTiles[i].y);
		xPos = tiles[yIndex][xIndex]->GetX() + (tiles[yIndex][xIndex]->GetWidth() / 2);
		yPos = tiles[yIndex][xIndex]->GetY() + (tiles[yIndex][xIndex]->GetHeight() / 2);
		radius = tiles[yIndex][xIndex]->GetWidth() / 15;

		DrawCircle(xPos, yPos, radius, color);
	}
}
void Spawner::ReplacedDestoyedBlocks()
{	
	short xIndex = 0;
	short yIndex = 0;
	Vector2 pos = {0,0};

	for(short i = 0; i < selectedTiles.size(); i++)
	{
		xIndex = static_cast<short>(selectedTiles[i].x);
		yIndex = static_cast<short>(selectedTiles[i].y);
		pos = {selectedTiles[i].x, selectedTiles[i].y};

		tiles[yIndex][xIndex] = SetUpTile(pos, screenWidth, screenHeight, xSeparation, ySeparation);
	}	
}

void Spawner::ResetShifts()
{
	for(short i = 0; i < maxShiftsIndex; i++)
	{
		shifts[i] = 48;
	}
}
void Spawner::AddShift()
{
	shifts[maxShiftsIndex - 1]++;

	if (shifts[maxShiftsIndex - 1] > 57)
	{
		shifts[maxShiftsIndex - 1] = 48;
		shifts[maxShiftsIndex - 2]++;
	}
}
char* Spawner::GetShifts()
{
	return shifts;
}
char Spawner::GetFirstValueOfArray()
{
	return shifts[0];
}
char Spawner::GetSecondValueOfArray()
{
	return shifts[1];
}
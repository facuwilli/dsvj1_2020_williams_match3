#include "tile_class.h"

char Tile::tilesDestroyed[maxTilesIndex] = { 48, 48 };

Tile::Tile()
{
	SetRectangle(0,0,0,0);	
	SetColor(WHITE);
	SetType(TileType::blue);	
}
Tile::~Tile()
{
	AddTileDestroyed();
}

void Tile::SetRectangle(float _x, float _y, float _width, float _height)
{
	rec.x = _x;
	rec.y = _y;
	rec.width = _width;
	rec.height = _height;
}
void Tile::SetX(float x)
{
	rec.x = x;
}
void Tile::SetY(float y)
{
	rec.y = y;
}
void Tile::SetWidth(float width)
{
	rec.width = width;
}
void Tile::SetHeight(float height) 
{
	rec.height = height;
}
void Tile::SetColor(Color _color)
{
	color = _color;
}
void Tile::SetColor(TileType type)
{
	switch (type)
	{
	case TileType::red:
		color = RED;
		break;
	case TileType::green:
		color = GREEN;
		break;
	case TileType::blue:
		color = BLUE;
		break;
	case TileType::yellow:
		color = YELLOW;
		break;
	default:
		break;
	}
}
void Tile::SetType(TileType _type)
{
	type = _type;
}
void Tile::SetTexture(Texture2D texture)
{
	tileTexture = texture;
}

TileType Tile::GenerateRandomType()
{
	TileType type = TileType::blue;
	short random = GetRandomValue(1, 4);
	
	switch(random)
	{
	case 1:
		type = TileType::red;
		break;
	case 2:
		type = TileType::green;
		break;
	case 3:
		type = TileType::blue;
		break;
	case 4:
		type = TileType::yellow;
		break;
	}	

	return type;
}
Rectangle Tile::GetRectangle()
{
	return rec;
}
float Tile::GetX()
{
	return rec.x;
}
float Tile::GetY()
{
	return rec.y;
}
float Tile::GetWidth()
{
	return rec.width;
}
float Tile::GetHeight()
{
	return rec.height;
}
Color Tile::GetColor()
{
	return color;
}
TileType Tile::GetType()
{
	return type;
}
Texture2D Tile::GetTexture()
{
	return tileTexture;
}

bool Tile::IsMouseAbove()
{
	if(GetX() < GetMousePosition().x && GetX() + GetWidth() > GetMousePosition().x
		&& GetY() < GetMousePosition().y && GetY() + GetHeight() > GetMousePosition().y)
	{
		return true;
	}
	else 
	{
		return false;
	}
}

void Tile::AddTileDestroyed()
{
	tilesDestroyed[maxTilesIndex-1]++;

	for (short i = maxTilesIndex - 1; i > 0; i--)
	{
		if(tilesDestroyed[i] > 57)
		{
			tilesDestroyed[i] = 48;
			tilesDestroyed[i - 1]++;
		}
	}
}
void Tile::ResetTilesDestroyed()
{
	for (short i = 0; i < maxTilesIndex; i++)
	{
		tilesDestroyed[i] = 48;
	}
}
char* Tile::GetTilesDestroyed()
{
	return tilesDestroyed;	
}
char Tile::GetFirstValueOfArray()
{
	return tilesDestroyed[0];
}
char Tile::GetSecondValueOfArray()
{
	return tilesDestroyed[1];
}
char Tile::GetThirdValueOfArray()
{
	return tilesDestroyed[2];
}